# Problem 1
## Author: Marin Benčević

With Python's array slicing I simply set every other row or column of the image to 0, using `image[::2, :]` for horizontal lines and `image[:, ::2]` for vertical lines.

![](sudoku_H.png)

![](sudoku_V.png)