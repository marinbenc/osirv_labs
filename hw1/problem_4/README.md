# Problem 4
## Author: Marin Benčević

This assignment is very similar to Problem 3, except this time we invert the squares by subtracting their values from 255.

```python
def checkerboard(img, width = 32):
  ret = img.copy()
  i = 0 # checkerboard row number
  j = 0 # current y pixel
  while i < len(img):
    while j < len(img[0]):
      # invert the square colors by subtracting from 255
      # this line is the only difference from Problem 3
      ret[i*width:i*width+width, j:j+width] = 255 - ret[i*width:i*width+width, j:j+width]
      j += width * 2
    i += 1
    # every other row starts with an offset
    j = 0 if i % 2 == 0 else width
  return ret
```

![](airplane.png)

![](boats.png)

![](sudoku.png)