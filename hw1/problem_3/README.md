# Problem 3
## Author: Marin Benčević

I used a similar approach as in Problem 2 to add a checkerboard pattern. We go row by row, adding black squares. Every other row stars with a y offset of `width` pixels.

```python
def checkerboard(img, width = 32):
  ret = img.copy()
  i = 0 # checkerboard row number
  j = 0 # current y pixel
  while i < len(img):
    while j < len(img[0]):
      ret[i*width:i*width+width, j:j+width] = 0
      j += width * 2
    i += 1
    # every other row starts with an offset
    j = 0 if i % 2 == 0 else width
  return ret
```

![](airplane.png)

![](boats.png)

![](sudoku.png)