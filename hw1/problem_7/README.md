# Problem 7
## Author: Marin Benčević

I first converted the image to YCbCr color space. I then subsampled the Cb and Cr channels in the same way as in Problem 6. This creates an image where the Cb and Cr spaces are smaller by a factor of `f` while the Y channel is the same size. Which means we need to scale up the Cb and Cr channels, and not the whole image like we did in Problem 6.

```python
def subsample(img, f):
  # switch to ycbcr
  copy = img.copy()
  copy = cv2.cvtColor(copy, cv2.COLOR_BGR2YCR_CB)
  copy = crop_nearest_multiple(copy, f)
  original = copy.copy()

  f2 = 1.0 / (f * f)
  
  # subsample chroma channels
  for i in range(len(copy) // f):
    for j in range(len(copy[i]) // f):
      copy[i, j, 1] = f2 * np.sum(original[i*f:i*f+f, j*f:j*f+f, 1]) # Cr
      copy[i, j, 2] = f2 * np.sum(original[i*f:i*f+f, j*f:j*f+f, 2]) # Cb

  # scale up the subsampled channels to the whole image
  cr = copy[:len(copy) // f, :len(copy[0]) // f, 1]
  cr = np.kron(cr, np.ones((f, f)))
  cb = copy[:len(copy) // f, :len(copy[0]) // f, 2]
  cb = np.kron(cb, np.ones((f, f)))
  copy[:, :, 1] = cr
  copy[:, :, 2] = cb

  # back to bgr
  copy = cv2.cvtColor(copy, cv2.COLOR_YCR_CB2BGR)
  return copy
```

The resulting images have the same amount of detail as the original image, but we can see some artifacts with the colors on scale factor 16. In some places the colors are subsampled too much and we can clearly see rectangles of the same Cr and Cb values. On other scale factors the difference is almost unnoticable.

![](baboon_2_subs.png)

![](baboon_4_subs.png)

![](baboon_8_subs.png)

![](baboon_16_subs.png)

![](pepper_2_subs.png)

![](pepper_4_subs.png)

![](pepper_8_subs.png)

![](pepper_16_subs.png)

The maximum scale factor without noticable artefacts is around 8.

A regular image takes 8 x 8 x 8 bits per pixel. Since we sample every 8th Cr and Cb sample, we only have 1 bit of data per pixel for those channels. This means our final image needs only 8 x 1 x 1 = 8 bits per pixel.