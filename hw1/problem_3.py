import cv2
import utils

PROBLEM_FOLDER = "problem_3"

images, names = utils.load_images(utils.images_path, 0)

def checkerboard(img, width = 32):
  ret = img.copy()
  i = 0 # checkerboard row number
  j = 0 # current y pixel
  while i < len(img):
    while j < len(img[0]):
      ret[i*width:i*width+width, j:j+width] = 0
      j += width * 2
    i += 1
    # every other row starts with an offset
    j = 0 if i % 2 == 0 else width
  return ret

for i, image in enumerate(images):
  image = checkerboard(image)
  utils.save_image(image, PROBLEM_FOLDER, names[i])


