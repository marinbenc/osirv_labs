# Problem 6
## Author: Marin Benčević

First I defined two helper functions. One will crop the image to the nearest multiple of a number by subtracting the division remainder from the dimension.

```python
def crop_nearest_multiple(image, num):
  w = image.shape[0] - image.shape[0] % num
  h = image.shape[1] - image.shape[1] % num
  return image[0:w, 0:h]
```

The other helper function will scale up an image by a factor `f` by calculating the Kronecker product of the image and a matrix of ones with shape `(f, f)`. We need this function because the subsampling produces an image which is `f` times smaller than the original image. We will use the `scale` function to produce simulated subsampling, so we can easily compare quality of the images.

```python
def scale(img, f):
  new = img.copy()
  if len(img.shape) == 2:
    new = np.kron(img, np.ones((f,f)))
  else:
    new = np.kron(img, np.ones((f,f,1)))
  return new.astype("uint8")
```

Finally the function which does the subsampling. It first crops the image to a multiple of `f`. Then it creates a new image smaller by a factor of `f`. We then iterate trough this new image, and set each pixel to an average of the corresponding pixel's neighborhood in the larger image. For color images, we do the same but for each channel inidividually.

```python
def subsample(img, f):
  copy = img.copy()
  copy = crop_nearest_multiple(copy, f)
  f2 = 1.0 / (f * f)

  smaller_img = copy[0:copy.shape[0] // f, 0:copy.shape[1] // f]
  
  for i in range(len(smaller_img)):
    for j in range(len(smaller_img[i])):
      if len(copy.shape) == 2:
        smaller_img[i, j] = f2 * np.sum(img[i*f:i*f+f, j*f:j*f+f])
      else:
        for k in range(copy.shape[2]):
          smaller_img[i, j, k] = f2 * np.sum(img[i*f:i*f+f, j*f:j*f+f, k])

  return smaller_img
```

![](baboon_sub8.png)

![](baboon_sub16.png)

![](baboon_sub32.png)

![](pepper_sub8.png)

![](pepper_sub16.png)

![](pepper_sub32.png)