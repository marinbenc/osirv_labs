import utils
import numpy as np
import os
import cv2

PROBLEM_FOLDER = "problem_7"

def crop_nearest_multiple(image, num):
  w = image.shape[0] - image.shape[0] % num
  h = image.shape[1] - image.shape[1] % num
  return image[0:w, 0:h]

def subsample(img, f):
  # switch to ycbcr
  copy = img.copy()
  copy = cv2.cvtColor(copy, cv2.COLOR_BGR2YCR_CB)
  copy = crop_nearest_multiple(copy, f)
  original = copy.copy()

  f2 = 1.0 / (f * f)
  
  # subsample chroma channels
  for i in range(len(copy) // f):
    for j in range(len(copy[i]) // f):
      copy[i, j, 1] = f2 * np.sum(original[i*f:i*f+f, j*f:j*f+f, 1]) # Cr
      copy[i, j, 2] = f2 * np.sum(original[i*f:i*f+f, j*f:j*f+f, 2]) # Cb

  # scale up the subsampled channels to the whole image
  cr = copy[:len(copy) // f, :len(copy[0]) // f, 1]
  cr = np.kron(cr, np.ones((f, f)))
  cb = copy[:len(copy) // f, :len(copy[0]) // f, 2]
  cb = np.kron(cb, np.ones((f, f)))
  copy[:, :, 1] = cr
  copy[:, :, 2] = cb

  # back to bgr
  copy = cv2.cvtColor(copy, cv2.COLOR_YCR_CB2BGR)
  return copy

names = ["baboon", "pepper"]
paths = [os.path.join(utils.images_path, name + ".bmp") for name in names]
images = [cv2.imread(path) for path in paths]

def save(img, name, f):
  base_path = os.path.join(utils.PROJECT_PATH, "hw1", PROBLEM_FOLDER)
  save_path = os.path.join(base_path, name + "_" + str(f) + "_subs.png")
  print("![](" + name + "_" + str(f) + "_subs.png)")
  cv2.imwrite(save_path, img)

for i, image in enumerate(images):
  for f in [2, 4, 8, 16]:
    image = subsample(image, f)
    save(image, names[i], f)