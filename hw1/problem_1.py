import utils

PROBLEM_FOLDER = "problem_1"

images, names = utils.load_images(utils.images_path, 1)

for i, image in enumerate(images):
  v = image.copy()
  v[:, ::2] = 0
  utils.save_image(v, PROBLEM_FOLDER, names[i], "_V")
  h = image.copy()
  h[::2, :] = 0
  utils.save_image(h, PROBLEM_FOLDER, names[i], "_H")


