# Problem 5
## Author: Marin Benčević

I perform Floyd-Steinberg dithering by iterating trough the image, quantising every pixel and then adding the errors to neighbouring pixels. I had to perform if checks to make sure there are no out of bounds errors. The image had to be casted to float32 to add the error to the values.

The quality if these images is much better than when only performing quantisation, as this gives "smooth" looking transitions (at least from far away).

```python
def dither(image, q):
  img = image.copy()
  img = img.astype("float32")
  d = pow(2.0, 8.0 - q)
  
  for i in range(len(img)):
    for j in range(len(img[i])):
      # quantisation step
      quantised = (np.floor(img[i][j] / d) + 1/2.0) * d
      e = img[i, j] - quantised
      img[i, j] = quantised

      # if last pixel
      if i + 1 == len(img) and j + 1 == len(img[i]):
        continue

      # if last row
      if i + 1 == len(img):
        img[i-1][j+1] += 3.0/16.0 * e
        img[i][j+1] += 5.0/16.0 * e
        continue
      
      # if last column
      if j + 1 == len(img[i]):
        img[i+1][j] += 7.0/16.0 * e
        continue

      img[i+1][j] += 7.0/16.0 * e
      img[i+1][j + 1] += 1.0/16.0 * e
      img[i-1][j+1] += 3.0/16.0 * e
      img[i][j + 1] += 5.0/16.0 * e
  img = img.astype("uint8")
  return img
```

![](goldhill_2fsd.png)

![](pepper_3fsd.png)

![](lenna_2fsd.png)