import cv2
import utils
import numpy as np

PROBLEM_FOLDER = "problem_5"

images, names = utils.load_images(utils.images_path)

def dither(image, q):
  img = image.copy()
  img = img.astype("float32")
  d = pow(2.0, 8.0 - q)
  
  for i in range(len(img)):
    for j in range(len(img[i])):
      # quantisation step
      quantised = (np.floor(img[i][j] / d) + 1/2.0) * d
      e = img[i, j] - quantised
      img[i, j] = quantised

      # if last pixel
      if i + 1 == len(img) and j + 1 == len(img[i]):
        continue

      # if last row
      if i + 1 == len(img):
        img[i-1][j+1] += 3.0/16.0 * e
        img[i][j+1] += 5.0/16.0 * e
        continue
      
      # if last column
      if j + 1 == len(img[i]):
        img[i+1][j] += 7.0/16.0 * e
        continue

      img[i+1][j] += 7.0/16.0 * e
      img[i+1][j + 1] += 1.0/16.0 * e
      img[i-1][j+1] += 3.0/16.0 * e
      img[i][j + 1] += 5.0/16.0 * e
  img = img.astype("uint8")
  return img

for i, image in enumerate(images):
  for q in [1, 2, 3]:
    image = dither(image, q)
    utils.save_image(image, PROBLEM_FOLDER, names[i], "_" + str(q) + "fsd")
