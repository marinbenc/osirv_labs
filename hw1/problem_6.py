import utils
import numpy as np
import os
import cv2

PROBLEM_FOLDER = "problem_6"

def crop_nearest_multiple(image, num):
  w = image.shape[0] - image.shape[0] % num
  h = image.shape[1] - image.shape[1] % num
  return image[0:w, 0:h]

def scale(img, f):
  new = img.copy()
  if len(img.shape) == 2:
    new = np.kron(img, np.ones((f,f)))
  else:
    new = np.kron(img, np.ones((f,f,1)))
  return new.astype("uint8")

def subsample(img, f):
  copy = img.copy()
  copy = crop_nearest_multiple(copy, f)
  f2 = 1.0 / (f * f)

  smaller_img = copy[0:copy.shape[0] // f, 0:copy.shape[1] // f]
  
  for i in range(len(smaller_img)):
    for j in range(len(smaller_img[i])):
      if len(copy.shape) == 2:
        smaller_img[i, j] = f2 * np.sum(img[i*f:i*f+f, j*f:j*f+f])
      else:
        for k in range(copy.shape[2]):
          smaller_img[i, j, k] = f2 * np.sum(img[i*f:i*f+f, j*f:j*f+f, k])

  return smaller_img

names = ["baboon", "pepper"]
paths = [os.path.join(utils.images_path, name + ".bmp") for name in names]
images = [cv2.imread(path) for path in paths]

def save(img, name, f):
  base_path = os.path.join(utils.PROJECT_PATH, "hw1", PROBLEM_FOLDER)
  save_path = os.path.join(base_path, name + "_sub" + str(f) + ".png")
  print("![](" + name + "_sub" + str(f) + ".png)")
  cv2.imwrite(save_path, img)


for i, image in enumerate(images):
  for f in [8, 16, 32]:
    image = scale(subsample(image, f), f)
    save(image, names[i], f)

  
