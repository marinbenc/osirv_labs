import cv2
import numpy as np
import os

####### IMPORTANT: #####################################################
# Input the absolute path to your project here
# Your path will probably be "/home/osirv/osirv_labs/" or something similar
# depending on where you cloned your repo
# This way paths in your program don't depend on your current working directory
PROJECT_PATH = "/Users/marinbenc/Documents/Projects/Faks/osirv_labs/"

images_path = os.path.join(PROJECT_PATH, "images")

def load_images(images_path, grayscale=1):
    """ Outputs: list of loaded images as numpy arrays and list of image filenames"""
    images = []
    image_names = []
    # os.listdir returns a list of filenames in a directory passed as parameter
    for image in os.listdir(images_path):
        image_path = os.path.join(images_path, image)
        if grayscale:
            open_as_flag = cv2.IMREAD_GRAYSCALE
        else:
            open_as_flag = cv2.IMREAD_COLOR
        images.append(cv2.imread(image_path, open_as_flag))
        image_names.append(image)
    return images, image_names

def show(image):
    # Break if trying to show image that is not uint8 datatype. You should always
    # convert your images to np.uint8 before trying to display them
    assert image.dtype == np.uint8, "Image is not np.uint8 datatype, convert to appropriate datatype"

    cv2.imshow("Image", image)
    cv2.waitKey()
    cv2.destroyAllWindows()

def save_image(image, folder, image_name, append_string=""):
    # Split the string on '.' character and take all splits except the last one 
    # The last split should be the extension. This implementation works with files 
    # which have variable extension size (e.g. jpg and jpeg), but requires that file 
    # has an extension. Creates a list of strings from the input string, split on places
    # where '.' character appears
    base_name = image_name.split('.')[:-1]
    # Joins the list of strings from previous splitting
    base_name = "".join(base_name)
    print("Base name for " + image_name + " is " + base_name)

    # Create absolute path to the project
    base_path = os.path.join(PROJECT_PATH, "hw1", folder)
    save_path = os.path.join(base_path, base_name + append_string + ".png")
    print("Saving the image to: " + save_path)
    cv2.imwrite(save_path, image)

