import cv2
import utils

PROBLEM_FOLDER = "problem_2"

images, names = utils.load_images(utils.images_path, 0)

def stripify(img, width = 10, vertical = False):
  ret = img.copy()
  i = 0
  if vertical:
    while i < len(img[0]):
      ret[:, i:i+width] = 0
      i += width * 2
  else:
    while i < len(img):
      ret[i:i+width, :] = 0
      i += width * 2
  return ret

widths = [8, 16, 32]

for i, image in enumerate(images):
  for w in widths:
    h = stripify(image, w)
    utils.save_image(h, PROBLEM_FOLDER, names[i], "_H" + str(w))
    v = stripify(image, w, True)
    utils.save_image(v, PROBLEM_FOLDER, names[i], "_V" + str(w))