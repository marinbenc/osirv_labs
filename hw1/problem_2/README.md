
# Problem 2
## Author: Marin Benčević

Using Pyhton's array slicing, we slice from index n to index n + width and set the slice to zero, and increment n by width * 2. We do this while n is smaller than the image's dimension in the direction we're interating trough. This splits the image into bars of size `width` and sets every other bar to black. We do this inside a function so we can easily change the parameters.

```
def stripify(img, width = 10, vertical = False):
  ret = img.copy()
  i = 0
  if vertical:
    while i < len(img[0]):
      ret[:, i:i+width] = 0
      i += width * 2
  else:
    while i < len(img):
      ret[i:i+width, :] = 0
      i += width * 2
  return ret
```

![](sudoku_H8.png)

![](sudoku_H32.png)

![](sudoku_V8.png)

![](sudoku_V32.png)