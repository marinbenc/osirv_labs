# LV5

Author: Marin Benčević

## Problem 1

The lower the upper treshold, the more edges there are on screen. Lower edges controls the small fine edges. The best results are with both the lower and upper treshold at 128.

![](problem_1/canny_0_255.png)

![](problem_1/canny_128_128.png)

![](problem_1/canny_255_255.png)

## Problem 2

Saving image airplane with thresholds (134,255)
Saving image barbara with thresholds (72,143)
Saving image boats with thresholds (92,183)
Saving image pepper with thresholds (81,160)

## Problem 3

The best results are with theta = 90, threshold = 150.

![](problem_3/chess_90_150.png)

![](problem_3/chess_180_200.png)

![](problem_3/chess_90_200.png)

![](problem_3/chess_180_150.png)

## Problem 4

![](problem_4/chess_1.png)

![](problem_4/chess_3.png)

![](problem_4/chess_5.png)

## Problem 5

![](problem_5/features.png)

![](problem_5/matched.png)