import cv2
import numpy as np
from matplotlib import pyplot as plt

img = cv2.imread('../../slike/lenna.bmp', 0)

tresholds = [(0, 255), (128, 128), (255, 255)]

for lower, upper in tresholds:
    edges = cv2.Canny(img, lower, upper)
    cv2.imwrite('canny_' + str(lower) + "_" + str(upper) + ".png", edges)
