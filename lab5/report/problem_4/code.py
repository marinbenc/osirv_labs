import cv2
import numpy as np
from matplotlib import pyplot as plt
import copy

img = cv2.imread('../../slike/chess.jpg')

for block_size in [1, 3, 5]:
    
    img2 = copy.copy(img)
    img2 = cv2.cvtColor(img,cv2.COLOR_BGR2RGB)
    gray = cv2.cvtColor(img,cv2.COLOR_BGR2GRAY)
    gray = np.float32(gray)

    dst = cv2.cornerHarris(gray,block_size,3,0.04)

    #result is dilated for marking the corners, not important
    dst = cv2.dilate(dst,None)

    # threshold for an optimal value, it may vary depending on the image.
    img2[dst>0.01*dst.max()]=[0,0,255]

    img2 = cv2.cvtColor(img2, cv2.COLOR_BGR2RGB)
    cv2.imwrite('chess_' + str(block_size) + '.png', img2)


