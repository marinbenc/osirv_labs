import numpy as np
import argparse
import glob
import cv2
from matplotlib import pyplot as plt

def auto_canny(image, sigma=0.33):
        # compute the median of the single channel pixel intensities
        v = np.median(image)

        # apply automatic Canny edge detection using the computed median
        lower = int(max(0, (1.0 - sigma) * v))
        upper = int(min(255, (1.0 + sigma) * v))
        edged = cv2.Canny(image, lower, upper)
        print "Median lower: %r." % lower
        print "Median upper: %r." % upper
        # return the edged image
        return edged, lower, upper

images = ['airplane', 'barbara', 'boats', 'pepper']

for image_name in images:
    image = cv2.imread("../../slike/" + image_name + ".bmp", 0)
    auto, lower, upper = auto_canny(image)
    print("Saving image " + image_name + " with thresholds (" + str(lower) + "," + str(upper) + ")")
    cv2.imwrite(image_name + "_" + str(lower) + "_" + str(upper) + ".png", auto)
