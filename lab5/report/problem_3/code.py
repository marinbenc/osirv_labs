import cv2
import numpy as np
import math
import copy
from matplotlib import pyplot as plt

img = cv2.imread('../../slike/chess.jpg')

params = [(90, 150), (180, 200), (90, 200), (180, 150)]

for theta, threshold in params:
    img2 = copy.copy(img)
    img2 = cv2.cvtColor(img,cv2.COLOR_BGR2RGB)
    gray=cv2.cvtColor(img,cv2.COLOR_BGR2GRAY)
    edges = cv2.Canny(gray, 166, 255)

    lines = cv2.HoughLines(edges, 1, math.pi/theta, threshold, np.array([]), 0, 0)

    a,b,c = lines.shape
    for i in range(a):
        rho = lines[i][0][0]
        theta_ = lines[i][0][1]
        a = math.cos(theta_)
        b = math.sin(theta_)
        x0, y0 = a*rho, b*rho
        pt1 = ( int(x0+1000*(-b)), int(y0+1000*(a)) )
        pt2 = ( int(x0-1000*(-b)), int(y0-1000*(a)) )
        cv2.line(img2, pt1, pt2, (255, 0, 0), 2, cv2.LINE_AA)

    img2 = cv2.cvtColor(img2, cv2.COLOR_BGR2RGB)
    cv2.imwrite('chess' + '_' + str(theta) + '_' + str(threshold) + '.png', img2)



