import numpy as np
import cv2 as cv

def clean(image):
    image[image > 255] = 255
    image[image < 0] = 0
    return image.astype('uint8')

def show(image):
    image = clean(image)
    image[image > 255] = 255
    image[image < 0] = 0
    cv.imshow('image', image)
    cv.waitKey(0)
    cv.destroyAllWindows()

def save(image, name):
    image = clean(image)
    cv.imwrite(name, image)

image = cv.imread('../images/baboon.bmp')

# blue = image.copy()
# blue[:,:,1:] = 0
# red = image.copy()
# red[:,:,:2] = 0
# green = image.copy()
# green[:,:,::2] = 0

# save(red, 'crvena.jpg')
# save(green, 'zelena.jpg')
# save(blue, 'plava.jpg')

borderedShape = (image.shape[0] + 20, image.shape[1] + 20, image.shape[2])
bordered = np.zeros(borderedShape, image.dtype)
bordered[10:bordered.shape[0] - 10, 10:bordered.shape[1] - 10, :] = image

save(bordered, 'bordered.jpg')

smaller = image[::2, :, :]
evenSmaller = image[:, ::2, :]
smallest = image[::2, ::2, :]

save(smaller, 'smaller.jpg')
save(evenSmaller, 'evenSmaller.jpg')
save(smallest, 'smallest.jpg')

