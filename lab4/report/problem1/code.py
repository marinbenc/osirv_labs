#!/usr/bin/env python
# -*- coding: utf-8 -*-

import numpy as np
import cv2
import matplotlib.pyplot as plt

def gaussian_noise(img, mu, sigma):
  out = img.astype(np.float32)
  noise = np.random.normal(mu, sigma, img.shape)
  out = out + noise
  out[out<0] = 0
  out[out>255] = 255
  return out.astype(np.uint8)

def salt_n_pepper_noise(img, percent=10):
  out = img.astype(np.float32)
  limit = ((float(percent)/2.0)/100.0) * 255.0
  noise = np.random.uniform(0,255, img.shape)
  out[noise<limit] = 0
  out[noise>(255-limit)] = 255
  out[out>255] = 255
  out[out<0] = 0
  return out.astype(np.uint8)

def uniform_noise(img, low, high):
  out = img.astype(np.float32)
  noise = np.random.uniform(low,high, img.shape)
  out = out + noise
  out[out>255] = 255
  out[out<0] = 0
  return out.astype(np.uint8)

def clean(image):
    image[image > 255] = 255
    image[image < 0] = 0
    return image.astype('uint8')

def save(image, name):
    image = clean(image)
    cv2.imwrite(name, image)

def show(img):
  cv2.imshow("title", img)
  cv2.waitKey(0)
  cv2.destroyAllWindows()

def showhist(img):
  hist,bins = np.histogram(img.flatten(), bins=256, range=(0,255))
  plt.vlines(np.arange(len(hist)), 0, hist)
  plt.title("Histogram")

img = cv2.imread("../../../images/lenna.bmp", cv2.IMREAD_GRAYSCALE)

def saveImagesAndShowHist(i, images, title):
    plt.figure(i)
    for j in range(len(images)):
        plt.subplot(3, 3, j + 1)
        showhist(images[j])
        save(images[j], title + str(j) + ".png")
    plt.show()

sigmas = [1,5,10,20,40,60]
gaussian = [gaussian_noise(img, 0, sigma) for sigma in sigmas]
saveImagesAndShowHist(0, gaussian, "gaussian")

bounds = [(-20,20),(-40,40),(-60,60)]
uniform = [uniform_noise(img, bound[0], bound[1]) for bound in bounds]
saveImagesAndShowHist(1, uniform, "uniform")

percents = [0.05, 0.1, 0.15, 0.2]
salt = [salt_n_pepper_noise(img, percent) for percent in percents]
saveImagesAndShowHist(1, salt, "saltAndPepper")