#!/usr/bin/env python
# -*- coding: utf-8 -*-

import numpy as np
import cv2
import matplotlib.pyplot as plt

def gaussian_noise(img, mu, sigma):
  out = img.astype(np.float32)
  noise = np.random.normal(mu, sigma, img.shape)
  out = out + noise
  out[out<0] = 0
  out[out>255] = 255
  return out.astype(np.uint8)

def salt_n_pepper_noise(img, percent=10):
  out = img.astype(np.float32)
  limit = ((float(percent)/2.0)/100.0) * 255.0
  noise = np.random.uniform(0,255, img.shape)
  out[noise<limit] = 0
  out[noise>(255-limit)] = 255
  out[out>255] = 255
  out[out<0] = 0
  return out.astype(np.uint8)

def uniform_noise(img, low, high):
  out = img.astype(np.float32)
  noise = np.random.uniform(low,high, img.shape)
  out = out + noise
  out[out>255] = 255
  out[out<0] = 0
  return out.astype(np.uint8)

def clean(image):
    image[image > 255] = 255
    image[image < 0] = 0
    return image.astype('uint8')

def save(image, name):
    image = clean(image)
    cv2.imwrite(name, image)

def show(img):
  cv2.imshow("title", img)
  cv2.waitKey(0)
  cv2.destroyAllWindows()

def showhist(img):
  hist,bins = np.histogram(img.flatten(), bins=256, range=(0,255))
  plt.vlines(np.arange(len(hist)), 0, hist)
  plt.title("Histogram")

def median(img, size):
    out = img.copy()
    for i in range(len(img)):
        for j in range(len(img[i])):
            startI = 0 if i - size < 0 else i - size
            endI = len(img - 1) if i + size > len(img) - 1 else i + size
            startJ = 0 if j - size < 0 else j - size
            endJ = len(img[i] - 1) if j + size > len(img[i]) - 1 else j + size
            hood = img[startI:endI, startJ:endJ]
            out[i][j] = np.median(hood)

    return out



images = ['boats.bmp', 'airplane.bmp']

for image in images:
    img = cv2.imread("../../../images/" + image, cv2.IMREAD_GRAYSCALE)
    sigmas = [5, 15, 35]
    gauss = [gaussian_noise(img, 0, sigma) for sigma in sigmas]

    percents = [0.01, 0.1]
    salt = [salt_n_pepper_noise(img, percent) for percent in percents]

    for i in range(len(gauss)):
        save(gauss[i], "corrupted_gauss_" + image + str(i) + ".png")

    for i in range(len(gauss)):
        save(gauss[i], "corrupted_salt_" + image + str(i) + ".png")

    for i, img in enumerate(gauss + salt):
        medianRadii = [1, 5]
        for radius in medianRadii:
            filtered = cv2.medianBlur(img, radius)
            save(filtered, "filtered_median_" + image + str(i) + "_" + str(radius) + ".png")

        blurSigmas = [1, 5]
        for sigma in blurSigmas:
            filtered = cv2.GaussianBlur(img, (5, 5), sigma)
            save(filtered, "filtered_gauss_" + image + str(i) + "_" + str(sigma) + ".png")

img = cv2.imread("../../../images/" + "boats.bmp")
myMedian = median(img, 3)
save(myMedian, "myMedian.png")