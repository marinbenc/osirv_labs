# LV4

## Zadatak 1

Prikazali smo slike koje smo pokvarili razlicitim sumovima. Na histogramu vidimo da svi osim salt and pepper suma "zagladjuju" i smanjuju histogram, odnosno boje postaju vise uniformne, dok salt and pepper ne utjece previse na histogram.

![](problem1/gaussian1.png)

![](problem1/saltAndPepper1.png)

![](problem1/uniform1.png)

## Zadatak 2

Koristili smo median i gaussian blur za otklanjanje suma. Median bolje radi, pogotovo za salt and pepper sum, iako izgubi informacije slike.

![](problem2/corrupted_gauss_airplane.bmp2.png)

![](problem2/corrupted_salt_airplane.bmp2.png)

![](problem2/filtered_gauss_airplane.bmp4_5.png)

![](problem2/filtered_median_airplane.bmp4_5.png)

Na kraju smo implementirali median filter.

```
def median(img, size):
    out = img.copy()
    for i in range(len(img)):
        for j in range(len(img[i])):
            startI = 0 if i - size < 0 else i - size
            endI = len(img - 1) if i + size > len(img) - 1 else i + size
            startJ = 0 if j - size < 0 else j - size
            endJ = len(img[i] - 1) if j + size > len(img[i]) - 1 else j + size
            hood = img[startI:endI, startJ:endJ]
            out[i][j] = np.median(hood)

    return out
```

Sa ovakvim rezultatom:

![](problem2/myMedian.png)

## Zadatak 3

Pokvratili smo 3 slike sa gaussovskim sumom, i onda smo vrsili 3 razlicita tresholda na slikama. Najbolje je sum uklonio Otsu thresholding nakon zamucivanja slike, ali se izgubilo puno podataka iz slike.

![](problem3/boats_original.png)

![](problem3/boats_filtered_global.png)

![](problem3/boats_filtered_otsu.png)

![](problem3/boats_filtered_otsugauss.png)