import cv2
import numpy as np
from matplotlib import pyplot as plt

def save(image, name):
  cv2.imwrite(name, image)

def gaussian_noise(img, mu, sigma):
  out = img.astype(np.float32)
  noise = np.random.normal(mu, sigma, img.shape)
  out = out + noise
  out[out<0] = 0
  out[out>255] = 255
  return out.astype(np.uint8)

# global thresholding

# plot all the images and their histograms

imageNames = ['boats', 'baboon', 'airplane']

for name in imageNames:
  img = cv2.imread('../../../images/' + name + '.bmp', cv2.IMREAD_GRAYSCALE)
  img = gaussian_noise(img, 0, 30)
  save(img, name + "_original.png")

  # global thresholding
  ret1,th1 = cv2.threshold(img,127,255,cv2.THRESH_BINARY)

  # Otsu's thresholding
  ret2,th2 = cv2.threshold(img,0,255,cv2.THRESH_BINARY+cv2.THRESH_OTSU)

  # Otsu's thresholding after Gaussian filtering
  blur = cv2.GaussianBlur(img,(5,5),0)
  ret3,th3 = cv2.threshold(blur,0,255,cv2.THRESH_BINARY+cv2.THRESH_OTSU)

  images = [th1, th2, th3]
  methods = ['global', 'otsu', 'otsugauss']

  for i, image in enumerate(images):
    save(image, name + "_filtered_" + methods[i] + ".png")