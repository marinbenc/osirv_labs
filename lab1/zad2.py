import random as r

def streetNames():
    print("Upisite imena ulica (7 - 15 znakova) i \"prekid\" kad ste gotovi.")
    names = []
    
    while True:
        name = input()
        if name == "prekid":
            break
        length = len(name)
        if length < 7 or length > 15:
            print("Ime nije izmedju 7 i 15 znakova!")
            continue
        names.append(name)

    print("Uneseno je", len(names), "imena.")

    if len(names) == 0:
        return
    
    names.sort(key=lambda n: len(n), reverse=True)
    print("Najduze je ime", names[0])

streetNames()

        
