import random as r

def guessInput():
    n = r.randrange(15)
    while int(input("Upišite broj: ")) != n:
        continue

    print("Bravo! Pogodili ste!")

guessInput();
