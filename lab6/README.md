# Lab 6 - Video Analysis and Introduction to Machine Learning with OpenCV 

<h3>Exercises</h3>

You need to create the `report` directory and the file `README.md` inside of it.  <br>
`README.md` file should contain Your task-solving and excersise reports.



<h2>6.1. Read, Write and Display Video files</h2>

A video is a sequence of fast moving images. The obvious question that follows is how fast are the pictures moving? 
The measure of how fast the images are transitioning is given by a metric called frames per second(FPS). 
ach image represents a frame, so if a video is captured and played back at 24fps, that means each second of video shows 24 distinct still images. 
The speed at which they’re shown tricks your brain into perceiving smooth motion.

Why does frame rate matter?

Frame rate greatly impacts the style and viewing experience of a video. Different frame rates yield different viewing experiences, and choosing a frame rate often means choosing 
between things such as how realistic you want your video to look, or whether or not you plan to use techniques such as slow motion or motion blur effects.

For example, movies are usually displayed at 24fps, since this frame rate is similar to how we see the world, and creates a very cinematic look. 
Video that’s broadcast live or video with a lot of motion, such as a sporting event or video game recording, will often have a higher frame rate, 
as there’s a lot happening at once and a higher frame rate keeps the motion smooth and the details crisp. 
On the other hand, people who create animated GIFs will often sacrifice detail for a smaller file size and choose a low frame rate.

<p align="center">
<img src="https://i.ibb.co/tHbMmrw/frames.png" alt="frames" border="0">
</p>

How to choose the best frame rate for video?

First of all, there is no best frame rate. 
As I pointed out above, different frame rates yield different results, so to choose the 
best one means going with the option that best fits what you’re trying to create.
To help figure out which frame rate is best for you, let’s look at a few common options and how they’re used.
<ul>
<li> 24fps  - this is the minimum speed needed to capture video while still maintaining realistic motion. 
If you capture a really busy scene at 24fps, you’ll see a lot of motion blur. This is the standard for movies and TV shows, 
and it was determined to be the minimum speed needed to capture video while still maintaining realistic motion. 
Even if a film is shot at a higher frame rate, it’s often produced and displayed at 24fps. Most feature films and TV shows are shot and viewed at 24 fps. </li>
<li> 30fps – six more frames, cause appearance of more details during scenes with high motion, however, the motion may start to look a little unnatural and suffer from the “soap opera effect.”.
This has been the standard for television since the early days, and is still widely used despite producers moving toward a more cinematic 24fps. 
Videos with a lot of motion, such as sports, will often benefit from the extra frames per second.    </li>
<li> 60+fps – anything higher than 30fps is mainly used to create slow motion video or to record video game footage.   </li>

</ul>

<h4> Excersise 1 </h4>

<b>Try out the following code in order to read video `road.mp4`. </b>

OpenCV implementation offor video reading: 

```
Function : cv2.VideoCapture(device)
Parameters are as follows :
1. if used device is camera, this parameter should be 0, while if reading from file it should be path to the vide
```
More information can be found at:  <a href="https://docs.opencv.org/2.4/modules/highgui/doc/reading_and_writing_images_and_video.html">OpenCV cv2.VideoCapture documentation</a> 

```
import cv2
import numpy as np

# Create a VideoCapture object and read from input file
# If the input is the camera, pass 0 instead of the video file name
cap = cv2.VideoCapture('road.mp4')

# Check if camera opened successfully
if (cap.isOpened()== False):
  print("Error opening video stream or file")

# Read until video is completed
while(cap.isOpened()):
  # Capture frame-by-frame
  ret, frame = cap.read()
  if ret == True:

    # Display the resulting frame
    cv2.imshow('Frame',frame)
    k = cv2.waitKey(30) & 0xff
    if k == 27:
        break
cap.release()
#destroy all the frames
cv2.destroyAllWindows()

```

<h4> Excersise 2 </h4>

<b>Try out one of the following code in order to save video `road.mp4`. </b>

More information can be found at:  <a href="https://docs.opencv.org/2.4/modules/highgui/doc/reading_and_writing_images_and_video.html">OpenCV documentation</a> 


```
import numpy as np
import cv2

cap = cv2.VideoCapture('road.mp4')

# define the codec and create VideoWriter object
fourcc = cv2.VideoWriter_fourcc(*'XVID')
out = cv2.VideoWriter('output.avi',fourcc, 20.0, (640,480))

while(cap.isOpened()):
    ret, frame = cap.read()
    if ret==True:
        frame = cv2.flip(frame,180)

        # write the flipped frame
        out.write(frame)

        # Display the resulting frame
        cv2.imshow('Frame',frame)
        k = cv2.waitKey(30) & 0xff
        if k == 27:
             break
# release everything if job is finished
cap.release()
out.release()
cv2.destroyAllWindows()

```

<h4> Tasks </h4>
<ul>
<li>Change the above code to flip video horizontally. </li>
</ul>

<hr />

<h3>Optical Flow</h3>

Optical flow or optic flow is the pattern of apparent motion of objects, surfaces, and edges in a visual scene caused by the relative motion between an observer and a scene.
It is represented as 2D vector field where each vector is a displacement vector showing the movement of points from first frame to second. 

Following image shows a ball moving in 5 consecutive frames. The arrow shows its displacement vector. Optical flow has many applications in areas like :
<p align="centagr">
<img src="https://i.ibb.co/zQjbB0v/opticflow.png" alt="opticflow" border="0">
</p>

The Optical Flow methods try to estimate the direction and speed of object motion from one image to another or from one video frame to another using either 
the Horn-Schunck or the Lucas-Kanade method.


<hr /> 

<h4>Lucas-Kanade Method</h4>

To solve the optical flow constraint equation for ![opticfl](http://latex.codecogs.com/gif.latex?u) and and ![opticalflow1](http://latex.codecogs.com/gif.latex?v), 
the Lucas-Kanade method divides the original image into smaller sections and assumes a constant velocity in each section. 
Then, it performs a weighted least-square fit of the optical flow constraint equation to a constant model for ![optical3](http://latex.codecogs.com/gif.latex?%5Bu%20v%5D%5E%7BT%7D) in 
each section ![optical4](http://latex.codecogs.com/gif.latex?%5COmega) . 
The method achieves this fit by minimizing the following equation: 

<p align="center">
![optical5](http://latex.codecogs.com/gif.latex?%5Csum_%7B%7D%5E%7Bx%3D%5COmega%20%7D) ![optical6](http://latex.codecogs.com/gif.latex?W%5E%7B%5E%7B2%7D%7D%20%5C%2C%20%5BI_xu%20&plus;%20I_yv&plus;I_t%5D%5E%7B%5E%7B2%7D%7D)
</p>

W is a window function that emphasises the constraints at the center of each section. The solution to the minimization problem is therefore: 
<p align="center">
<img src="https://i.ibb.co/MVWwLcf/Screenshot-from-2018-12-19-08-53-10.png" alt="Screenshot-from-2018-12-19-08-53-10" border="0">
</p>

What does this mean formally?
A good features have big eigenvalues which implies appereance of texture or corner. 

It is important to mention that traditional Lucas-Kanade typically runs on small, corner-like features (e.g. 5x5) to compute optic flow, but there's no reason we can't use the same
approach on a larger window around the object being tracked. 

<b>Try out the following code for optical flow tracking on video `road.mp4`. </b>


Information about OpenCV implementation of Lucas-Kanade method for optical flow
can be found at:  <a href="https://docs.opencv.org/3.0-beta/modules/video/doc/motion_analysis_and_object_tracking.html">OpenCV cv2.calcOpticalFlowPyrLK documentation</a> 


```
import numpy as np
import cv2

cap = cv2.VideoCapture('road.mp4')

# params for ShiTomasi corner detection
feature_params = dict( maxCorners = 15,
                       qualityLevel = 0.3,
                       minDistance = 7,
                       blockSize = 7 )

# Parameters for lucas kanade optical flow
lk_params = dict( winSize  = (15,15),
                  maxLevel = 2,
                  criteria = (cv2.TERM_CRITERIA_EPS | cv2.TERM_CRITERIA_COUNT, 10, 0.03))

# Create some random colors
color = np.random.randint(0,255,(100,3))

# Take first frame and find corners in it
ret, old_frame = cap.read()
old_gray = cv2.cvtColor(old_frame, cv2.COLOR_BGR2GRAY)
p0 = cv2.goodFeaturesToTrack(old_gray, mask = None, **feature_params)

# Create a mask image for drawing purposes
mask = np.zeros_like(old_frame)

while(1):
    ret,frame = cap.read()
    frame_gray = cv2.cvtColor(frame, cv2.COLOR_BGR2GRAY)

    # calculate optical flow
    p1, st, err = cv2.calcOpticalFlowPyrLK(old_gray, frame_gray, p0, None, **lk_params)

    # Select good points
    good_new = p1[st==1]
    good_old = p0[st==1]

    # draw the tracks
    for i,(new,old) in enumerate(zip(good_new,good_old)):
        a,b = new.ravel()
        c,d = old.ravel()
        mask = cv2.line(mask, (a,b),(c,d), color[i].tolist(), 2)
        frame = cv2.circle(frame,(a,b),5,color[i].tolist(),-1)
    img = cv2.add(frame,mask)

    cv2.imshow('frame',img)
    k = cv2.waitKey(30) & 0xff
    if k == 27:
        break

    # Now update the previous frame and previous points
    old_gray = frame_gray.copy()
    p0 = good_new.reshape(-1,1,2)

cv2.destroyAllWindows()
cap.release()


```


<hr />

<h4>Dense Optical Flow</h4>

While Lucas-Kanade method computes optical flow for a sparse feature set (in our example, corners detected using Shi-Tomasi algorithm). 
OpenCV provides another algorithm to find the dense optical flow. It computes the optical flow for all the points in the frame. 
It is based on Gunner Farneback's algorithm which is explained in 
<a href="http://www.diva-portal.org/smash/get/diva2:273847/FULLTEXT01.pdf">Two-Frame Motion Estimation Based on Polynomial Expansion</a> 


The next example shows how to find the dense optical flow using above algorithm. 
We get a 2-channel array with optical flow vectors, (u,v). We find their magnitude and direction. 
We color code the result for better visualization. Direction corresponds to hue value of the image and magnitude corresponds to value plane. 

<b>Try out the following code for dense optical flow tracking program on the video `road.mp4`. </b>

```
import cv2
import numpy as np
from matplotlib import pyplot as plt
import copy

img = cv2.imread('../slike/chess.jpg')
img2 = copy.copy(img)
img2 = cv2.cvtColor(img,cv2.COLOR_BGR2RGB)

gray = cv2.cvtColor(img,cv2.COLOR_BGR2GRAY)

gray = np.float32(gray)
dst = cv2.cornerHarris(gray,2,3,0.04)

#result is dilated for marking the corners, not important
dst = cv2.dilate(dst,None)

# threshold for an optimal value, it may vary depending on the image.
img2[dst>0.01*dst.max()]=[0,0,255]

plt.subplot(121),plt.imshow(cv2.cvtColor(img, cv2.COLOR_BGR2RGB))
plt.title('Original Image'), plt.xticks([]), plt.yticks([])
plt.subplot(122),plt.imshow(img2 , 'gray')
plt.title('Detected Corners'), plt.xticks([]), plt.yticks([])
plt.show()

```

<h4>Task</h4>
<ul>
<li> What is difference between sparse and dense optical flow methods?   </li>
</ul>


<h3>Haar Cascades</h3>

What are Haar features? 


A simple rectangular Haar-like feature can be defined as the difference of the sum of pixels of areas inside the rectangle, 
which can be at any position and scale within the original image. This modified feature set is called 2-rectangle feature. 
Viola and Jones also defined 3-rectangle features and 4-rectangle features. The values indicate certain characteristics of a particular area of the image.
Each feature type can indicate the existence (or absence) of certain characteristics in the image, such as edges or changes in texture. For example, 
a 2-rectangle feature can indicate where the border lies between a dark region and a light region. 


Object Detection using Haar feature-based cascade classifiers is an effective object detection method proposed by Paul Viola and Michael Jones in their paper, 
<a href="https://www.cs.cmu.edu/~efros/courses/LBMV07/Papers/viola-cvpr-01.pdf">Rapid Object Detection using a Boosted Cascade of Simple Features</a>
It is a machine learning based approach where a cascade function is trained from a lot of positive and negative images. It is then used to detect objects in other images.

Initially, the algorithm needs a lot of positive images (images of cars) and negative images (images without cars) to train the classifier. 
Then we need to extract features from it. For this, haar features shown in below image are used. 
They are just like our convolutional kernel. Each feature is a single value obtained by subtracting sum of pixels under white rectangle from sum of pixels under black rectangle.

So the, Haar Classifier starts by extracting Haar features from each image as shown by the windows below:

<p align="center">
<img src="https://i.ibb.co/ScFwJWT/haar3.png" alt="haar3" border="0">
</p>

Haar-Features are good at detecting edges and lines and because of that are often used for face detection. 

Following example shows car tracking with Haar Classifier. 

OpenCV implementation for Cascade Classifier can be found in: 

```
Function : cv2.CascadeClassifier('cascade_training.xml')
Parameters are as follows :
1. cascade_training.xml' - input training file (can be done with Haar method, or some else like LBP)

```

More information can be found at:  <a href="https://docs.opencv.org/2.4.13.2/modules/objdetect/doc/cascade_classification.html?highlight=cascadeclassifier">OpenCV cv2.CascadeClassifier documentation</a>.
Tutorial for cascade training (with wich `cars.xml ` was trained) can be found <a href="https://docs.opencv.org/3.1.0/dc/d88/tutorial_traincascade.html"> Cascade Classifier</a>

Another important function is 

```
Function: cv.detectMultiScale(image,scaleFactor,minNeighbors)
1. the input is the grayscale image
2. scaleFactor: this function compensates a false perception in size that occurs when one face appears to be bigger than the other simply because it is closer to the camera
3. minNeighbors: this is a detection algorithm that uses a moving window to detect objects, it does so by defining how many objects are found 
near the current one before it can declare the face found.

```

More information can be found at:  <a href="https://docs.opencv.org/2.4.13.2/modules/objdetect/doc/cascade_classification.html#cv2.CascadeClassifier.detectMultiScale">OpenCV cv2.CascadeClassifier documentation</a> 


<b>Try out the following code for basic cascade detection program on video `road.mp4`. </b>

```
import cv2 

face_cascade = cv2.CascadeClassifier('cars.xml')
vc = cv2.VideoCapture('road.mp4')

if vc.isOpened():
    rval , frame = vc.read()
else:
    rval = False

while rval:
    rval, frame = vc.read()

    # car detection.
    cars = face_cascade.detectMultiScale(frame, 1.1, 2)

    ncars = 0
    for (x,y,w,h) in cars:
        cv2.rectangle(frame,(x,y),(x+w,y+h),(0,0,255),2)
        ncars = ncars + 1

    # show result
    cv2.imshow("Result",frame)
    cv2.waitKey(1);
vc.release()
```

As you may observe, this will detect cars in the screen but also noise and the screen will be jittering sometimes. 
To avoid all of these, we have to improve our car tracking algorithm. Algorithm can be improved as follows: 

for every frame we need to:
<ul>
<li> detect potential regions of interest </li>
<li> filter detected regions based on vertical,horizontal similarity</li>
<li> if its a new region, add to the collection </li>
<li> clear collection every 30 frames </li>
</ul>

```
import cv2
import numpy as np
 
def diffUpDown(img):
    # compare top and bottom size of the image
    # 1. cut image in two
    # 2. flip the top side
    # 3. resize to same size
    # 4. compare difference  
    height, width, depth = img.shape
    half = height/2
    top = img[0:half, 0:width]
    bottom = img[half:half+half, 0:width]
    top = cv2.flip(top,1)
    bottom = cv2.resize(bottom, (32, 64)) 
    top = cv2.resize(top, (32, 64))  
    return ( mse(top,bottom) )
 
 
def diffLeftRight(img):
    # compare left and right size of the image
    # 1. cut image in two
    # 2. flip the right side
    # 3. resize to same size
    # 4. compare difference  
    height, width, depth = img.shape
    half = width/2
    left = img[0:height, 0:half]
    right = img[0:height, half:half + half-1]
    right = cv2.flip(right,1)
    left = cv2.resize(left, (32, 64)) 
    right = cv2.resize(right, (32, 64))  
    return ( mse(left,right) )
 
 
def mse(imageA, imageB):
    err = np.sum((imageA.astype("float") - imageB.astype("float")) ** 2)
    err /= float(imageA.shape[0] * imageA.shape[1])
    return err
 
def isNewRoi(rx,ry,rw,rh,rectangles):
    for r in rectangles:
        if abs(r[0] - rx) < 40 and abs(r[1] - ry) < 40:
           return False  
    return True
 
def detectRegionsOfInterest(frame, cascade):
    scaleDown = 2
    frameHeight, frameWidth, fdepth = frame.shape 
 
    # Resize
    frame = cv2.resize(frame, (frameWidth/scaleDown, frameHeight/scaleDown)) 
    frameHeight, frameWidth, fdepth = frame.shape 
 
    # haar detection.
    cars = cascade.detectMultiScale(frame, 1.2, 1)
 
    newRegions = []
    minY = int(frameHeight*0.3)
 
    # iterate regions of interest
    for (x,y,w,h) in cars:
            roi = [x,y,w,h]
            roiImage = frame[y:y+h, x:x+w]   
 
            carWidth = roiImage.shape[0]
            if y > minY:
                diffX = diffLeftRight(roiImage)
                diffY = round(diffUpDown(roiImage))
 
                if diffX > 1600 and diffX < 3000 and diffY > 12000:
                    rx,ry,rw,rh = roi
                    newRegions.append( [rx*scaleDown,ry*scaleDown,rw*scaleDown,rh*scaleDown] )
 
    return newRegions
 
def detectCars(filename):
    rectangles = []
    cascade = cv2.CascadeClassifier('cars.xml')
    vc = cv2.VideoCapture(filename)
 
    if vc.isOpened():
        rval , frame = vc.read()
    else:
        rval = False
 
    roi = [0,0,0,0]
    frameCount = 0
 
    while rval:
        rval, frame = vc.read()
        frameHeight, frameWidth, fdepth = frame.shape 
 
        newRegions = detectRegionsOfInterest(frame, cascade)
        for region in newRegions:
            if isNewRoi(region[0],region[1],region[2],region[3],rectangles):
                rectangles.append(region)
 
        for r in rectangles:
            cv2.rectangle(frame,(r[0],r[1]),(r[0]+r[2],r[1]+r[3]),(0,0,255),3) 
 
        frameCount = frameCount + 1
        if frameCount > 30: 
            frameCount = 0
            rectangles = []
 
        # show result
        cv2.imshow("Result",frame)
        cv2.waitKey(1);
    vc.release()
 
detectCars('road.mp4')
```

It is important to mention that the cascades are not rotation invariant, scale and translation invariant. 
In addition, detecting vehicles with haar cascades may work reasonably well, but there is gain with other algorithms (such as salient points).


<hr />
<h4>Tasks </h4>
<ul>
<li>Try to capture video from the camera.</li> 
</ul>


<hr />

HOHOHO !!!
See ya after praznici. 

</xmp>

<body>
<script src="http://strapdownjs.com/v/0.2/strapdown.js"></script>
<script type="text/javascript"
  src="http://cdn.mathjax.org/mathjax/latest/MathJax.js?config=TeX-AMS-MML_HTMLorMML">
</script>
<script type="text/x-mathjax-config">MathJax.Hub.Config({ tex2jax: { inlineMath: [['$','$'], ['\\(','\\)']], processEscapes: true } });</script>
</html>
