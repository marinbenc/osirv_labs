# LV 6

## Problem 1

By changing the argument of the "flip" function we can flip the video horizontally.

```
frame = cv2.flip(frame, 0)
```

## Problem 2

Dense optical flow gets motion vectors of every pixel, while sparse optical flow only calculates motion vectors of some features like corners.

## Problem 3

To capture the camera we can use the following code:

```
cap = cv2.VideoCapture(0)
```