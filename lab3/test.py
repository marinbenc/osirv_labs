import numpy as np
import cv2 as cv

def clean(image):
    image[image > 255] = 255
    image[image < 0] = 0
    return image.astype('uint8')

def show(image):
    image = clean(image)
    image[image > 255] = 255
    image[image < 0] = 0
    cv.imshow('image', image)
    cv.waitKey(0)
    cv.destroyAllWindows()

def save(image, name):
    image = clean(image)
    cv.imwrite(name, image)

# get images
files = ['baboon', 'barbara', 'lenna']
images = [cv.imread('../../images/' + file + '.bmp') for file in files]

# crop all to same size
shapes = [im.shape for im in images]
minX = min([shape[0] for shape in shapes])
minY = min([shape[1] for shape in shapes])
images = [image[:minX, :minY, :] for image in images]

# stack them
stacked = np.hstack((images[0], images[1], images[2]))
# show(stacked)
save(stacked, 'stacked.png')

baboon = images[0]
print(baboon.shape)
bordered = np.zeros(baboon.shape)