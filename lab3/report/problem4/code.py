import numpy as np
import cv2 as cv
import glob
import os

def clean(image):
    image[image > 255] = 255
    image[image < 0] = 0
    return image.astype('uint8')

def show(image):
    image = clean(image)
    image[image > 255] = 255
    image[image < 0] = 0
    cv.imshow('image', image)
    cv.waitKey(0)
    cv.destroyAllWindows()

def save(image, name):
    image = clean(image)
    cv.imwrite(name, image)

files = glob.glob(os.path.join('../../../images/','*'))
print(files)

thresholds = [63, 127, 191]

for threshold in thresholds:
    for i, file in enumerate(files):
        image = cv.imread(file, cv.IMREAD_GRAYSCALE)
        image[image <= threshold] = 0
        save(image, 'image' + str(i) + '_' + str(threshold) + '_thresh.png')