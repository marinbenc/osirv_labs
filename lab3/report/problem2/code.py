import cv2 as cv
import numpy as np

def clean(image):
    image[image > 255] = 255
    image[image < 0] = 0
    return image.astype('uint8')

def show(image):
    image = clean(image)
    image[image > 255] = 255
    image[image < 0] = 0
    cv.imshow('image', image)
    cv.waitKey(0)
    cv.destroyAllWindows()

def save(image, name):
    image = clean(image)
    cv.imwrite(name, image)

def convolve(image, kernel):
    output = np.zeros( (image.shape[0] -kernel.shape[0] + 1,
                        image.shape[1] - kernel.shape[1] +1))
    kernel_rev = kernel[::-1,::-1]


    for i in range(0, output.shape[0]):
        for j in range(0, output.shape[1]):
            for k in range(kernel.shape[0]):
                for l in range(kernel.shape[1]):
                    output[i,j] += image[i+k,j+l] * kernel_rev[k,l]

    output[output>255] = 255
    output[output<0]   = 0
    output = output.astype(np.uint8)
    return output 


kernels = np.array([
    [[0, 0, 0], [0, 1, 0], [0, 0, 0]],
    [[1, 0, -1], [0, 0, 0], [-1, 0, 1]],
    [[0, 1, 0], [1, -4, 1], [0, 1, 0]],
    [[-1, -1, -1], [-1, 8, -1], [-1, -1, -1]],
    [[0, -1, 0], [-1, 5, -1], [0, -1, 0]],
    [[1, 1, 1], [1, 1, 1], [1, 1, 1]],
    [[1, 2, 1], [2, 4, 2], [1, 2, 1]]
], dtype=float)

blurKernels = np.array([
    [[1, 4, 6, 4, 1], [4, 16, 24, 16, 4], [6, 24, 36, 24, 6], [4, 16, 24, 16, 4], [1, 4, 6, 4, 1]],
    [[1, 4, 6, 4, 1], [4, 16, 24, 16, 4], [6, 24, -476, 24, 6], [4, 16, 24, 16, 4], [1, 4, 6, 4, 1]]
], dtype=float)

kernels[5] *= 1.0/9.0
kernels[6] *= 1/16.0
blurKernels[0] *= 1/256.0
blurKernels[1] *= -1/256.0

image = cv.imread('../../../images/lenna.bmp', cv.IMREAD_GRAYSCALE)

for i, kernel in enumerate(kernels):
    kernel = np.array(kernel)
    convolved = convolve(image, kernel)
    save(convolved, 'kernel' + str(i) + '.png')

for i, kernel in enumerate(blurKernels):
    kernel = np.array(kernel)
    convolved = convolve(image, kernel)
    save(convolved, 'kernel' + str(i + 7) + '.png')