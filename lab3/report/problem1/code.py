import numpy as np
import cv2 as cv

def clean(image):
    image[image > 255] = 255
    image[image < 0] = 0
    return image.astype('uint8')

def show(image):
    image = clean(image)
    image[image > 255] = 255
    image[image < 0] = 0
    cv.imshow('image', image)
    cv.waitKey(0)
    cv.destroyAllWindows()

def save(image, name):
    image = clean(image)
    cv.imwrite(name, image)

files = ['baboon', 'barbara', 'lenna']
images = [cv.imread('../../../images/' + file + '.bmp') for file in files]
shapes = [im.shape for im in images]
minX = min([shape[0] for shape in shapes])
minY = min([shape[1] for shape in shapes])
images = [image[:minX, :minY, :] for image in images]
stacked = np.hstack((images[0], images[1], images[2]))

# show(stacked)
save(stacked, 'stacked.png')

def addBorder(image, width):
    borderedShape = (image.shape[0] + width * 2, image.shape[1] + width * 2, image.shape[2])
    bordered = np.zeros(borderedShape)
    bordered[width:borderedShape[0]-width, width:borderedShape[1]-width, :] = image

baboon = images[0]
bordered = addBorder(baboon, 10)
save(bordered, 'bordered.png')

