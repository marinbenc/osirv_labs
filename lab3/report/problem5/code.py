import numpy as np
import cv2 as cv
import math

def clean(image):
    image[image > 255] = 255
    image[image < 0] = 0
    return image.astype('uint8')

def show(image):
    image = clean(image)
    image[image > 255] = 255
    image[image < 0] = 0
    cv.imshow('image', image)
    cv.waitKey(0)
    cv.destroyAllWindows()

def save(image, name):
    image = clean(image)
    cv.imwrite(name, image)

image = cv.imread('../../../images/BoatsColor.bmp', cv.IMREAD_GRAYSCALE)
image = image.astype(np.float32)

qs = [1, 2, 3, 4]

for q in qs:
    current = image.copy()
    d = pow(2.0, 8.0 - q)
    for i in range(current.shape[0]):
        for j in range(current.shape[1]):
            current[i, j] = (math.floor(current[i, j] / d) + 1/2.0) * d

    save(current, 'boats_' + str(q) + '.bmp')
