# LV3

## Problem 1

Koristenjem hstack smo poredali slike na jednu sliku. Prije toga smo ih sve cropali na iste dimenzije.

```
shapes = [im.shape for im in images]
minX = min([shape[0] for shape in shapes])
minY = min([shape[1] for shape in shapes])
images = [image[:minX, :minY, :] for image in images]
stacked = np.hstack((images[0], images[1], images[2]))
```

![](problem1/stacked.png)

Kreirali smo obrub oko slike tako sto smo kreirali novu crnu sliku koja je uvecana za velicinu okvira, i onda na sredinu te slike pridodijelili vrijednost nase slike.

```
def addBorder(image, width):
    borderedShape = (image.shape[0] + width * 2, image.shape[1] + width * 2, image.shape[2])
    bordered = np.zeros(borderedShape)
    bordered[width:borderedShape[0]-width, width:borderedShape[1]-width, :] = image
```

![](problem1/bordered.png)

## Problem 2

Koristili smo funkciju `convolve` i predali joj razlicite kernele.

```
for i, kernel in enumerate(kernels):
    kernel = np.array(kernel)
    convolved = convolve(image, kernel)
    save(convolved, 'kernel' + str(i) + '.png')
```

Slike:

![](problem2/kernel0.png)

![](problem2/kernel1.png)

![](problem2/kernel2.png)

![](problem2/kernel3.png)

![](problem2/kernel4.png)

![](problem2/kernel5.png)

![](problem2/kernel6.png)

![](problem2/kernel7.png)

![](problem2/kernel8.png)

## Problem 3

Invertirali smo slike tako sto smo oduzeli njihovu vrijednost od 255.

```
image = cv.imread(file, cv.IMREAD_GRAYSCALE)
image = 255 - image
```

![](problem3/image0.png)

![](problem3/image1.png)

![](problem3/image8.png)

![](problem3/image11.png)

![](problem3/image12.png)

## Problem 4

Radili smo thresholding sa 3 razlicita thresholda tako sto smo postavili sve vrijednosti ispod thresholda na 0.

```
image[image <= threshold] = 0
```

![](problem4/image0_63_thresh.png)

![](problem4/image0_127_thresh.png)

![](problem4/image0_191_thresh.png)

![](problem4/image2_63_thresh.png)

![](problem4/image2_127_thresh.png)

![](problem4/image2_191_thresh.png)

## Problem 5

Kvantizirali smo sliku koristenjem formule za kvantizaciju za svaki piksel.

```
for q in qs:
    current = image.copy()
    d = pow(2.0, 8.0 - q)
    for i in range(current.shape[0]):
        for j in range(current.shape[1]):
            current[i, j] = (math.floor(current[i, j] / d) + 1/2.0) * d

    save(current, 'boats_' + str(q) + '.bmp')
```

![](problem5/boats_1.bmp)

![](problem5/boats_2.bmp)

![](problem5/boats_3.bmp)

![](problem5/boats_4.bmp)